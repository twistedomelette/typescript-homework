import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './src/styles/styles.css';
import { render } from './src/index';

render();
let pagePOPULAR = 1;
let pageUNCOMING = 1;
let pageTOP_RATED = 1;
const pageSEARCH = {page: 1, name: ''};
let pageStatus = 'popular';
const API_KEY = 'api_key=75f94b8e518553015b64eaca29e5449a';
const BASE_URL = 'https://api.themoviedb.org/3';
const API_URL_SEARCH: string = BASE_URL + '/search/movie?' + API_KEY + '&language=en-US&query=';
const API_URL_POPULAR: string = BASE_URL + '/movie/popular?' + API_KEY + '&language=en-US&page=';
const API_URL_UNCOMING: string = BASE_URL + '/movie/upcoming?' + API_KEY + '&language=en-US&page=';
const API_URL_TOP_RATED: string = BASE_URL + '/movie/top_rated?' + API_KEY + '&language=en-US&page=';
const IMG_URL = 'https://image.tmdb.org/t/p/w500';

const container: any = document.getElementById('film-container');
const randomMovie: any = document.getElementById('random-movie');
const form: any = document.getElementById('form');
const search: any = document.getElementById('search');
const btnradio: any = document.getElementsByName('btnradio');
const showMore: any = document.getElementById('load-more');
const favoriteMovies: any = document.getElementById('favorite-movies');

console.log(!localStorage.getItem('movie'))
console.log(localStorage.getItem('movie'));
favoriteMovies.innerHTML = '';
if (!localStorage.getItem('movie')) {
    localStorage.setItem('movie', JSON.stringify([]));
}
interface IMovie {
    id?: number,
    poster_path?: string,
    overview?: string,
    release_date?: string,
    original_title?: string
}

getMovies(API_URL_POPULAR+pagePOPULAR, pagePOPULAR)

function getMovies(url: string, page: number) {
    fetch(url).then(res => res.json()).then(data => {
        showMovies(data.results, page);
    })
}

function showMovies(data: Array<any>, page:number) {
    if (page === 1) {
        container.innerHTML = '';
        randMovie(data);
    }

    data.forEach(movie => {
        const MovieStat: IMovie = {
            id: movie.id,
            poster_path: movie.poster_path,
            overview: movie.overview,
            release_date: movie.release_date,
        }
        const raw = JSON.parse(<string>localStorage.getItem('movie'));
        const findId = raw.find((e: number) => e === MovieStat.id)
        let status = false
        findId ? status = true : status = false
        const movieEl = document.createElement('div');
        movieEl.classList.add('col-lg-3');
        movieEl.classList.add('col-md-4');
        movieEl.classList.add('col-12');
        movieEl.classList.add('p-2');
        movieEl.innerHTML = `
            <div class="card shadow-sm" id="${MovieStat.id}M">
                <img
                    id="img${MovieStat.id}"
                    src="${IMG_URL+MovieStat.poster_path}"
                />
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    stroke="red"
                    fill="${checkLike(status)}"
                    width="50"
                    height="50"
                    class="bi bi-heart-fill position-absolute p-2"
                    viewBox="0 -2 18 22"
                    id="movie${MovieStat.id}"
                >
                    <path
                        id="${MovieStat.id}"
                        fill-rule="evenodd"
                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                    />
                </svg>
                <div class="card-body">
                    <p id="p${MovieStat.id}" class="card-text truncate">
                        ${MovieStat.overview}
                    </p>
                     <div
                     class="
                        d-flex
                        justify-content-between
                        align-items-center
                        "
                     >
                        <small class="text-muted">${MovieStat.release_date}</small>
                     </div>
                </div>
            </div>
        `
        container.appendChild(movieEl);
        if (JSON.parse(<string>localStorage.getItem('movie')).find((el: number) => el === MovieStat.id) === MovieStat.id) {
            const movieEl = document.getElementById(`fav${MovieStat.id}`);
            if (movieEl === null && MovieStat.id !== undefined)
                addfavorMovie(MovieStat.id);
        }
        const path = document.getElementById(`${MovieStat.id}`)
        if (path !== null) path.addEventListener('click', (e) => {
            e.preventDefault();
            const svg = document.getElementById(`movie${MovieStat.id}`)

            if (svg !== null && svg.style.fill !== 'red') {
                status = true;
                const raw = JSON.parse(<string>localStorage.getItem('movie'));
                const a = raw.find((e: number) => e === MovieStat.id)
                if (!a) {
                    raw.push(MovieStat.id);
                    localStorage.setItem('movie', JSON.stringify([...raw]));
                }
                if (MovieStat.id !== undefined)
                    addfavorMovie(MovieStat.id);
                if (svg !== null) svg.style.fill = 'red';
            } else {
                status = false;
                const raw = JSON.parse(<string>localStorage.getItem('movie'));
                const newRaw = raw.filter((e: number) => e !== MovieStat.id)
                localStorage.setItem('movie', JSON.stringify([...newRaw]));
                if (MovieStat.id !== undefined) {
                    deletefavorMovie(MovieStat.id);
                    deletefavorMovie(MovieStat.id);
                }
                if (svg !== null) {
                    svg.style.fill = "#ff000078";
                }
            }
        })
    })
}

function randMovie(data: Array<any>) {
    randomMovie.innerHTML = '';
    const numMovie = Math.floor(Math.random() * 20);
    const movie = data[numMovie];
    const MovieStat: IMovie = {
        original_title: movie.original_title,
        overview: movie.overview
    }
    const movieEl = document.createElement('div');
    movieEl.classList.add('row');
    movieEl.classList.add('py-lg-5');
    movieEl.innerHTML = `
            <div
                class="col-lg-6 col-md-8 mx-auto"
                style="background-color: #2525254f"
            >
                <h1 id="random-movie-name" class="fw-light text-light">${MovieStat.original_title}</h1>
                <p id="random-movie-description" class="lead text-white">
                            ${MovieStat.overview}
                </p>
            </div>
        `
    randomMovie.appendChild(movieEl);
}

function addfavorMovie(id: number) {
    let movieSrc = document.getElementById(`img${id}`);
    // @ts-ignore
    const movieP = document.getElementById(`p${id}`).innerHTML;
    // @ts-ignore
    movieSrc = movieSrc.getAttribute('src');
    const movieEl = document.createElement('div');
    movieEl.classList.add('row');
    movieEl.classList.add('py-lg-5');
    movieEl.innerHTML = `
            <div id="fav${id}" class="card shadow-sm">
                        <img
                            src="${movieSrc}"
                        />
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            stroke="red"
                            fill="red"
                            width="50"
                            height="50"
                            class="bi bi-heart-fill position-absolute p-2"
                            viewBox="0 -2 18 22"
                        >
                            <path
                                fill-rule="evenodd"
                                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                            />
                        </svg>
                        <div class="card-body">
                            <p class="card-text truncate">
                                ${movieP}
                            </p>
                            <div
                                class="
                                    d-flex
                                    justify-content-between
                                    align-items-center
                                "
                            >
                                <small class="text-muted">2021-05-26</small>
                            </div>
                        </div>
            </div>
        `
    favoriteMovies.appendChild(movieEl);
}

function deletefavorMovie(id: number) {
    const favId = document.getElementById(`fav${id}`);
    if (favId !== null)
    favId.remove();
}

function checkLike(status: boolean): string {
    if (status) {
        return 'red'
    } else {
        return "#ff000078"
    }
}

btnradio.forEach((el: any) => {
    el.onchange = topicSort;
})

function topicSort(this: any):void {
    const name: string = this.id;
    pageStatus = name;
    switch (name) {
        case 'popular':
            pagePOPULAR = 1;
            getMovies(API_URL_POPULAR + pagePOPULAR, pagePOPULAR); break;
        case 'upcoming':
            pageUNCOMING = 1;
            getMovies(API_URL_UNCOMING + pageUNCOMING, pageUNCOMING); break;
        case 'top_rated':
            pageTOP_RATED = 1;
            getMovies(API_URL_TOP_RATED + pageTOP_RATED, pageTOP_RATED); break;
    }
}

showMore.onclick = loadMore;

function loadMore():void {
    switch (pageStatus) {
        case 'popular':
            pagePOPULAR++;
            getMovies(API_URL_POPULAR + pagePOPULAR, pagePOPULAR); break;
        case 'upcoming':
            pageUNCOMING++;
            getMovies(API_URL_UNCOMING + pageUNCOMING, pageUNCOMING); break;
        case 'top_rated':
            pageTOP_RATED++;
            getMovies(API_URL_TOP_RATED + pageTOP_RATED, pageTOP_RATED); break;
        case 'search':
            pageSEARCH.page++;
            getMovies(API_URL_SEARCH + pageSEARCH.name + '&page='+ pageSEARCH.page +'&include_adult=false', pageSEARCH.page); break;
    }
}

form.addEventListener('submit', (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    const searchName:string = search.value;
    if (searchName) {
        pageStatus = 'search'
        pageSEARCH.page = 1;
        pageSEARCH.name = searchName;
        getMovies(API_URL_SEARCH + searchName + '&page='+ pageSEARCH.page +'&include_adult=false', pageSEARCH.page);
    } else {
        topicSort();
    }
})



